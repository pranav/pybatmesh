# Hacking

Everyone can [hack][] pybatmesh. See below for how to hack.

## Reporting issues and suggesting ideas

To report a bug or suggest an idea, create a new issue at
<https://git.disroot.org/pranav/pybatmesh/issues>

While reporting a bug, you can add the debug messages to provide more
data. Run `journalctl -fu pybatmesh` on a terminal emulator (this could
take some time on some machines). Now on another one, type `sudo systemctl start pybatmesh.service` or whatever caused the error. Now copy the error
messages and paste it in the issue body along with the description.

## Improving documentation

The README and HACKING.md needs to be more beginner friendly. See #20.

## Contribute code

To push to this repo, you need your username to be in the contributors
list. Add your username to issue #8 to add you as a contributor. Before
each commit, update the CHANGELOG.md and `__version__` in
`pybatmesh/__init__.py`

## Packaging

Currently this program is only packaged for Arch Linux. pybatmesh needs
packages in GNU+Linux+systemd distributions such as Debian, Fedora,
openSUSE, and nixos. If you know/like to package it in your distro, post a
message to issue #6.

[hack]: https://catb.org/jargon/html/H/hack.html
