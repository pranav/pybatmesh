# This makefile uses setup.py under the hood. In debian, and therefore
# ubuntu, and in fedora, python and pip are symlinks to python2 and pip2,
# not python3. So we have to specify python as python3 by default.

PYTHON := python3
PIP := pip3

# This can be changed when creating a package for your POSIX distribution
DESTDIR:= /

all: build

# Build only when pybatmesh/__init__.py changes. We assume here that anyone
# creating a new commit will first update the __version__ in
# pybatmesh/__init__.py
build: pybatmesh/__init__.py
	$(PYTHON) setup.py build

install:
	$(PYTHON) setup.py install --root="$(DESTDIR)" --optimize=1 --skip-build

uninstall:
	$(PIP) uninstall -y pybatmesh
	rm -rf /usr/share/pybatmesh /usr/lib/systemd/system/pybatmesh.service
	@echo "make uninstall will not remove anything in /etc/pybatmesh" && \
	echo "Do 'sudo make purge' to remove it."

rpm:
	$(PYTHON) setup.py bdist_rpm --requires python3-dasbus,python3-systemd,iwd,systemd-networkd

# Remove config files, like apt purge. Purge should first do what
# uninstall does, so we add uninstall as a dependency to this rule.
purge: uninstall
	rm -rf /etc/pybatmesh

clean:
	rm -rf build *.egg-info **/__pycache__ dist
