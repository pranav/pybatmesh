# Changelog

## [v0.5.3][] - 2022-08-03

- Renamed to pybatmesh, to be more clear what the program does
- code cleanup

To update via the repo, do the following:

```sh
git remote set-url origin git@git.disroot.org:pranav/pybatmesh.git
# or
git remote set-url origin https://git.disroot.org/pranav/pybatmesh.git
# remove naxlnet and install pybatmesh
git pull && git checkout 38d0aa8ab5dd60f2febffda335df6df1ea143ab3 && make clean && sudo make purge && git checkout main && make && sudo make install
```

## [v0.5.2][] - 2021-12-10

- Optimised auto gateway mode selection (#25)

## [v0.5.1][] - 2021-10-22

- Fixed stopping at boot when GNOME starts location service (#21)
- Changes in directory structure

## [v0.5.0][] - 2021-10-10

- Better error messages
- Sets gateway mode automatically (to choose the best available
  connection). **This might cause problems with nodes running previous
  version of naxalnet** (#15)
- Cleanup before exit
- Closed #19

## [v0.4.0][] - 2021-09-20

- naxalnet is now a daemon! naxalnet will reconfigure the WiFi network
  every time a WiFi adapter is plugged in or removed (#14)
- **Logging**: logs to systemd journal when run from systemd, stderr
  otherwise (#13)
- New dependency `python-systemd`
- Fixed dependency order in systemd service
- Added `--verbose` argument

## [v0.3.0][] - 2021-08-19

- Support for arguments (#11)
- Configuration file support with fallback values (#11)
- Made messages more readable
- Improved documentation in docstrings
- Changed default name of mesh network. **This will make naxalnet
  incompatible with nodes running previous versions.**
- New versioning scheme that conforms to PEP 440

## [v0.2.0][] - 2021-07-26

- rfkill support (#9)
- rewrite into python module (#5)

## [v0.1.0][] - 2021-06-19

Rewrite to python. At first, this was a shell script. Then it was
converted into a single python file that did just what the shell script
used to do. The shell script was not given a version.

- Closed #1 and #3

[unreleased]: https://git.disroot.org/pranav/pybatmesh/compare/v0.5.3...HEAD
[v0.5.3]: https://git.disroot.org/pranav/pybatmesh/compare/v0.5.2...v0.5.3
[v0.5.2]: https://git.disroot.org/pranav/pybatmesh/compare/v0.5.1...v0.5.2
[v0.5.1]: https://git.disroot.org/pranav/pybatmesh/compare/v0.5.0...v0.5.1
[v0.5.0]: https://git.disroot.org/pranav/pybatmesh/compare/v0.4.0...v0.5.0
[v0.4.0]: https://git.disroot.org/pranav/pybatmesh/compare/v0.3.0...v0.4.0
[v0.3.0]: https://git.disroot.org/pranav/pybatmesh/compare/v0.2.0...v0.3.0
[v0.2.0]: https://git.disroot.org/pranav/pybatmesh/compare/v0.1.0...v0.2.0
[v0.1.0]: https://git.disroot.org/pranav/pybatmesh/releases/tag/v0.1.0
