# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
daemon.py
---------

The daemon part. This is used to perform some work when a new wifi adapter
is plugged in, or it is removed.
"""

from dasbus.loop import EventLoop
from pybatmesh.iwd import IWD, IWD_DEVICE_INTERFACE
from pybatmesh.log import logger


class Daemon:
    """implements the daemon part"""

    def __init__(self):
        self.loop = EventLoop()
        self.iwd = IWD()
        self.callback = None

    def on_device_add(self, path, data):
        """
        this function will be run every time a device is added
        """
        if IWD_DEVICE_INTERFACE in data:
            logger.debug("New device %s found", data[IWD_DEVICE_INTERFACE]["Name"])
            logger.info("Reloading")
            self.callback()

    def on_device_remove(self, path, data):
        """
        this function will be run every time a device is removed
        """
        if IWD_DEVICE_INTERFACE in data:
            logger.debug("A device was removed")
            logger.info("Reloading")
            self.callback()

    def add_callback(self, callback):
        """
        register the callback with D-Bus so that callback is
        run every time a device is added or removed
        """
        self.callback = callback
        proxy = self.iwd._proxy
        proxy.InterfacesAdded.connect(self.on_device_add)
        proxy.InterfacesRemoved.connect(self.on_device_remove)

    def start(self):
        """
        start the daemon
        """
        logger.debug("Starting daemon")
        self.loop.run()
