# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
default.py
----------

This file contains default values for configuration. This is taken as
fallback data by config.py if no configuration files were found, or if a
key-value pair was not present in the config file. The data will be
further changed by arguments if pybatmesh is called from the commandline.
See config.py for more info.
"""

CONFIG = {
    "networkd": {
        "confdir": "/usr/share/pybatmesh/networkd",
        "runtimedir": "/run/systemd/network",
    },
    "device": {"batman": "bat0", "bridge": "bridge0"},
    "adhoc": {"name": "pybatmesh"},
    "ap": {"ssid": "BatWiFi", "passwd": "pybatmesh256"},
    "gateway": {"mode": "auto"},
}

# glob
CONFIG_FILES = ["pybatmesh.conf", "pybatmesh.conf.d/*.conf"]
CONFIG_DIRS = ["/usr/share/pybatmesh", "/usr/local/share/pybatmesh", "/etc/pybatmesh"]
MESH_GLOB = "mesh.*"
TMP_NET_GLOB = "tmp.*"

ISSUE_URL = "https://git.disroot.org/pranav/pybatmesh/issues"
REPORT_BUG_INFO = "If you think this is a bug, report it to " + ISSUE_URL

# Changing this constant will alter not just the behaviour of pybatmesh,
# but affect the meaning of Life, the Universe and Everthing
ANSWER = 42
