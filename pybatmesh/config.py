# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
config.py
---------

This file contains functions to parse configuration files and arguments.
Most of these functions are meant to be used by parse_args() internally,
so only parse_args() should be imported outside this file.

Some parts of pybatmesh can be configured by configuration files and
arguments in a specific order. First, the default values from default.py
is taken. Then, key-value pairs from the configuration files are read, if
they exist, in the following order:

- First it reads /usr/share/pybatmesh/pybatmesh.conf and then from
  /usr/share/pybatmesh/pybatmesh.conf.d/*.conf where *.conf means any file
  with the name ending with ".conf". The files in this directory are
  intended to be used by distribution and package maintainers.

- Next, it does the same with /usr/local/share/pybatmesh

- Then, it looks for the files pybatmesh.conf and pybatmesh.conf.d/*.conf
  from the directory /etc/pybatmesh, like it did up above. This directory
  is where the user creates and stores the config file.

- Then it parses the arguments from the commandline, storing the values in
  the files parsed until now as fallback. Finally you get an
  argpase.Namespace object from parse_args(). Because of the way this is
  implemented, all key-value pairs in the configuration should have an
  argument too, or they won't be parsed.

All the key-value pairs are replaced successively if they exist by each
new configuration file parsed, similar to how systemd parses configuration
and service files. If any of the files checked does not exist, then they
are ignored and the next combination is checked. If none of the config
files exist and no arguments are given, the fallback data from default.py
is used.

"""

from pathlib import Path
from configparser import ConfigParser
from argparse import ArgumentParser, Namespace
from pybatmesh.default import CONFIG, CONFIG_FILES, CONFIG_DIRS


def get_config_files():
    """
    Read list of configuration files and return a list
    of files that exists as pathlib.Path objects
    """
    config_files = []

    for directory in CONFIG_DIRS:
        path = Path(directory)
        if path.exists():
            for i in CONFIG_FILES:
                glob = path.glob(i)
                config_files.extend(glob)
    return config_files


def parse_config():
    """
    Parse all configuration files, with the values in
    default.py as fallback
    """
    # logger.debug("Parsing config files")
    parser = ConfigParser()
    # encoded defaults
    parser.read_dict(CONFIG)
    # read config files
    files = get_config_files()
    for i in files:
        # logger.debug("Reading config file %s", str(i))
        parser.read_file(i.open())
    return parser


def parse_args() -> Namespace:
    """
    Parse all arguments and return ArgumentParser.parse_args(),
    with values in config files as fallback. Ideally, only this
    function should be used by pybatmesh to get arguments and
    configuration.
    """
    config = parse_config()
    parser = ArgumentParser(
        description="setup batman-adv networks with systemd and iwd"
    )
    parser.add_argument(
        "--ap-ssid",
        "-n",
        type=str,
        help="SSID of the WiFi AP",
        default=config["ap"]["ssid"],
    )
    parser.add_argument(
        "--ap-passwd",
        "-p",
        "--ap-password",
        type=str,
        help="password of the WiFi AP",
        default=config["ap"]["passwd"],
    )

    parser.add_argument(
        "--adhoc-name",
        "-a",
        type=str,
        default=config["adhoc"]["name"],
        help="name of adhoc network",
    )

    parser.add_argument(
        "--print-wifi",
        action="store_true",
        default=False,
        help="prints the ssid and password of the WiFi network and exit",
    )

    parser.add_argument(
        "--networkd-config-dir",
        type=str,
        default=config["networkd"]["confdir"],
        help="the directory where systemd-networkd configuration files are stored",
    )

    parser.add_argument(
        "--networkd-runtime-dir",
        type=str,
        default=config["networkd"]["runtimedir"],
        help="volatile directory where configuration files of systemd-networkd should be copied",
    )

    parser.add_argument(
        "--systemd",
        action="store_true",
        default=False,
        help="send log messages to systemd journal",
    )

    parser.add_argument(
        "--batman-device",
        default=config["device"]["batman"],
        help="name of interface used by batman-adv",
    )

    parser.add_argument(
        "--bridge-device",
        default=config["device"]["bridge"],
        help="name of bridge interface",
    )

    parser.add_argument(
        "--gateway-mode",
        default=config["gateway"]["mode"],
        help="auto, server, client, or off",
    )

    parser.add_argument(
        "--version",
        "-V",
        default=False,
        action="store_true",
        help="prints the version and exit",
    )

    parser.add_argument(
        "-v", "--verbose", action="count", default=0, help="increase output verbosity"
    )

    return parser.parse_args()


# This is defined here because log.py needs some arguments to determine
# the loglevel and where to send the log message to. If you know a better
# way of implementing it, create an issue
args = parse_args()
