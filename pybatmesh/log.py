# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
log.py
------

Initialise the logger for other submodules to import. Do not import any
submodules here other than pybatmesh.config, which is needed to set the
loglevel and to add the systemd journal handler
"""
import logging
from systemd.journal import JournalHandler
from pybatmesh.config import args


def get_logger():
    """
    Initialise the logger and return it.
    This function is meant to be used only by pybatmesh.log.
    If you want to import the logger, use:
    from pybatmesh.log import logger
    """
    log = logging.getLogger("pybatmesh")
    # --verbose
    if args.verbose >= 2:
        loglevel = logging.DEBUG
    elif args.verbose == 1:
        loglevel = logging.INFO
    else:
        loglevel = logging.WARNING

    # if --systemd is given, log to systemd journal
    if args.systemd:
        logging.basicConfig(level=logging.DEBUG)
        log.addHandler(JournalHandler())
    else:
        logging.basicConfig(level=loglevel)
    return log


logger = get_logger()
