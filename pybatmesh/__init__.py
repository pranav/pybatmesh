# This file is part of pybatmesh.
# Copyright (C) 2021 The pybatmesh Authors

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

"""
pybatmesh
========

Create mesh networks with batman-adv, systemd-networkd and iwd.

See README.md and the docstrings for documentation. You can read
documentaion in the python interpretor using something like the example
given below.

>>> help("pybatmesh")
>>> help("pybatmesh.config")

"""

# GUIDE FOR CHANGING __version__
#
# All commits in master should have the version as
# {last published version tag}.a{1,2,3,...}
# example: 0.2.0a3 should mean 3 commits after tag v0.2.0
#
# All commits in other branches should
# have {version in master}.dev{1,2,...}
# example: 0.2.0a3.dev1 should mean 1 commit in the new
# branch after the commit in master.
#
# In case you forgot to change the version, skip the number
# and put the next number in the next commit.
__version__ = "0.5.3"
